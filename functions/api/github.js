/**
 * POST /api/github.
 */

const BOT_TOKEN = "6232672560:AAH5kHfghdbISlKr185zccv7HeOKX5uIQ-w";
const WEBAPP_URL = "oproject.pages.dev/";
export async function onRequestPost(context) {
  try {
    let input = await context.request.json();
    
    const githubEvent = context.request.headers['x-github-event'];
    // Convert FormData to JSON
    // NOTE: Allows multiple values per key
    let pretty ;
    let tel;
    if(githubEvent=="push")
    {
      pretty = JSON.stringify(input.head_commit, null, 2);
      tel = await sendTelegramMessage("-1001463524522", pretty, "3776");
      
    }
    else
    {
      const { action, check_run: { name, status, conclusion } } = input;

      const pickedObject = { action, name, status, conclusion };
      const msg=`# Nuxt project \n- Name: *${name}*\n- Action: *${action}*\n- Status: *${status}*\n- Conclusion: *${conclusion}*`;
   	const dataString = `
*Nuxt project Updates*:  
- _action_: "${input.action}"
- *check_run*:
  - _name_: "${input.check_run.name}"
  - _status_: "${input.check_run.status}"
  - _conclusion_: "${input.check_run.conclusion}"
`;


      //pretty = JSON.stringify(pickedObject,null, 2);
      tel = await sendTelegramMessage("-1001463524522", dataString,"3776","Markdown");
      
    }
   
    return new Response(tel.body, {
      status: 200,
      statusText: "telegram api respond",
      headers: tel.headers,
    });
    
  } catch (err) {
    return new Response(err, { status: 400 });
  }
}


async function sendMessage(chatId, text) {
  const url = `https://api.telegram.org/bot${BOT_TOKEN}/sendMessage?chat_id=${chatId}&text=${encodeURIComponent(text)}`;
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Failed to send message to chat ${chatId}`);
  }
}

async function sendTelegramMessage(chatId, text, replyToMessageId = null,markdown=null) {
  const url = `https://api.telegram.org/bot${BOT_TOKEN}/sendMessage`;
  const payload = {
    chat_id: chatId,
    text: text,
    ...(replyToMessageId ? { reply_to_message_id: replyToMessageId }:{}),
    ...(markdown? {parse_mode: markdown}:{}),
  };

  return await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  });
}

/*

const githubEvent = request.headers['x-github-event'];

*/