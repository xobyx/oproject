const BOT_TOKEN = "6232672560:AAH5kHfghdbISlKr185zccv7HeOKX5uIQ-w";
const WEBAPP_URL = "oproject.pages.dev/";
//@xviiiiiibot
/**
 * POST /api/tele
 */
export async function onRequest(context) {
  try {
    const update = await context.request.json();

    if (update.message) {
      if (update.message.text === "hello") {
        await handleHelloCommand(update);
      } else if (update.message.web_app_data) {
        await handleWebAppData(update);
      } else {
        //await handleMessage(update);
      }
    } else if (update.callback_query) {
      await handleCallbackQuery(update.callback_query);
    } else if (update.edited_message) {
      await handleUnimplementedUpdateType(update, "edited_message");
    } else if (update.channel_post) {
      await handleUnimplementedUpdateType(update, "channel_post");
    } else if (update.edited_channel_post) {
      await handleUnimplementedUpdateType(update, "edited_channel_post");
    } else if (update.inline_query) {
      await handleUnimplementedUpdateType(update, "inline_query");
    } else if (update.chosen_inline_result) {
      await handleUnimplementedUpdateType(update, "chosen_inline_result");
    } else if (update.shipping_query) {
      await handleUnimplementedUpdateType(update, "shipping_query");
    } else if (update.pre_checkout_query) {
      await handleUnimplementedUpdateType(update, "pre_checkout_query");
    } else if (update.poll) {
      await handleUnimplementedUpdateType(update, "poll");
    } else if (update.poll_answer) {
      await handleUnimplementedUpdateType(update, "poll_answer");
    } else if (update.my_chat_member) {
      await handleUnimplementedUpdateType(update, "my_chat_member");
    } else if (update.chat_member) {
      await handleUnimplementedUpdateType(update, "chat_member");
    } else {
      await handleUnknownUpdate(update);
    }

    return new Response("ok");
  } catch (err) {
    return new Response(`Error: ${err}`, { status: 500 });
  }
}

async function sendMessage(chatId, text) {
  const url = `https://api.telegram.org/bot${BOT_TOKEN}/sendMessage?chat_id=${chatId}&text=${encodeURIComponent(text)}`;
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Failed to send message to chat ${chatId}`);
  }
}

async function sendTelegramMessage(chatId, text, replyToMessageId = null) {
  const url = `https://api.telegram.org/bot${BOT_TOKEN}/sendMessage`;
  const payload = {
    chat_id: chatId,
    text: text,
    ...(replyToMessageId && { reply_to_message_id: replyToMessageId }),
  };

  await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  });
}

async function handleHelloCommand(update) {
  const chatId = update.message.chat.id;
  const text = JSON.stringify(update, null, 2);
  const messageId = update.message.message_id;
  await sendTelegramMessage(chatId, text, messageId);
}

async function handleWebAppData(update) {
  const chatId = update.message.chat.id;
  const data = JSON.parse(update.message.web_app_data.data);
  let responseText = "Your data was:\n";
  data.forEach((item) => {
    responseText += `Name: ${item.name}, Value: ${item.value}\n`;
  });
  await sendMessage(chatId, responseText);
}

async function handleCallbackQuery(callbackQuery) {
  // Handle callback query logic here
}

async function handleUnimplementedUpdateType(update, updateType) {
  const chatId = update.message?.chat.id || update.callback_query?.message.chat.id;
  if (chatId) {
    const text = `Unimplemented update type: ${updateType}\n\n${JSON.stringify(update, null, 2)}`;
    await sendMessage(chatId, text);
  }
}

async function handleUnknownUpdate(update) {
  const chatId = update.message?.chat.id || update.callback_query?.message.chat.id;
  if (chatId) {
    const text = `Unknown update:\n\n${JSON.stringify(update, null, 2)}`;
    await sendMessage(chatId, text);
  }
}