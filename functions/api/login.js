
// Define the Telegram Bot Token
const BOT_TOKEN = '6232672560:AAH5kHfghdbISlKr185zccv7HeOKX5uIQ-w';

// Function to check Telegram authorization
async function checkTelegramAuthorization(authData) {
  const checkHash = authData.hash;
  delete authData.hash;

  const dataCheckArr = [];
  for (const key in authData) {
    dataCheckArr.push(`${key}=${authData[key]}`);
  }
  dataCheckArr.sort();
  const dataCheckString = dataCheckArr.join('\n');

  const encoder = new TextEncoder();
  const keyData = encoder.encode(BOT_TOKEN);
  const hashBuffer = await crypto.subtle.digest('SHA-256', keyData);
  
  const encoder4 = new TextEncoder();
  const dataCheckBuffer = encoder4.encode(dataCheckString);

  const importedKey = await crypto.subtle.importKey(
    'raw',
    hashBuffer,
    { name: 'HMAC', hash: { name: 'SHA-256' } },
    false,
    ['sign']
  );

  const signature = await crypto.subtle.sign('HMAC', importedKey, dataCheckBuffer);
  
  const signatureArray = Array.from(new Uint8Array(signature));
  const signatureHex = signatureArray.map(byte => byte.toString(16).padStart(2, '0')).join('');
  console.log(signatureHex === checkHash)
  

  if (signatureHex !== checkHash) {
    throw new Error('Data is NOT from Telegram');
  }

  if ((Date.now() / 1000 - authData.auth_date) > 86400) {
    throw new Error('Data is outdated');
  }

  return authData;
}

// Function to save Telegram user data
async function saveTelegramUserData(authData) {
  const authDataJson = JSON.stringify(authData);
  return new Response(authDataJson, {
    headers: {
      'Content-Type': 'application/json',
      'Set-Cookie': `tg_user=${authDataJson}; SameSite=Strict; Secure; HttpOnly`,
    },
  });
}

// Cloudflare Pages Function or Workers handler
export async function onRequest(context) {
  try {
    const url = new URL(context.request.url);
    const authData = Object.fromEntries(url.searchParams);
    const validatedAuthData = await checkTelegramAuthorization(authData);
    return saveTelegramUserData(validatedAuthData);
  } catch (e) {
    return new Response(e.message, { status: 400 });
  }
}

