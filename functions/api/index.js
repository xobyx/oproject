// Cloudflare Pages Function or Workers handler
export async function onRequest(context) {
  try {
        let pretty = JSON.stringify(context, null, 2);
    return new Response(pretty, {
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    });
  } catch (e) {
    return new Response(e.message, { status: 400 });
  }
}

