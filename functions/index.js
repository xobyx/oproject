// Cloudflare Pages Function or Workers handler
export async function onRequest(context) {
  try {
        let pretty = JSON.stringify(context, null, 2);
       return context.env.ASSETS.fetch(context.request);
    
  } catch (e) {
    return new Response(e.message, { status: 400 });
  }
}

